<?php
        header('Content-type: application/json');
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
        $request = json_decode(file_get_contents("php://input"));

        $to_email = 'info@itreports.am';
        $name = $request->name;
        $surName = $request->surName;
        $from_email = $request->email;
        $phone= $request->phone;
        $company= $request->company;
        $country= $request->country;
        $services= $request->services;
        $subject = $request->subject;

        $email_subject = "Hello";

        $email_body = '<html><body>';
        $email_body = "$<table style="box-shadow: 4px 8px 25px rgb(8 26 81 / 7%); background: white; max-width: 670px;width: 100%; margin: auto; padding: 18px;border-collapse: collapse">
          <tr>
            <td>
            <h1 style="margin-bottom: 10px; text-align: center;">Lorem ipsum</h1>
            <span style="display: block; border-bottom:1px solid rgb(206 206 206); width:100px; margin: 13px auto;"></span>
            </td>
          </tr>
          <tr>
            <td>
               <table style="border-collapse: collapse; width: 100%;  border: 1px solid rgb(237 237 237); max-width: 600px; margin: 35px auto;">
                   <tr>
                       <td style="padding:6px; border-bottom: 1px solid rgb(237 237 237); border-right: 1px solid rgb(237 237 237);">
                       <strong style="color: rgb(114 191 68)">Name: </strong></td>
                       <td style="padding:6px">$name</td>
                       </tr>
                       <tr>
                           <td style="padding:6px ; border-bottom: 1px solid rgb(237 237 237); border-right: 1px solid rgb(237 237 237);">
                            <strong style="color: rgb(114 191 68)">SurName: </strong></td>
                           <td style="padding:6px;border-bottom: 1px solid rgb(237 237 237);">$surName</td>
                       </tr>
                       <tr>
                          <td style="padding:6px ; border-bottom: 1px solid rgb(237 237 237); border-right: 1px solid rgb(237 237 237);">
                          <strong style="color: rgb(114 191 68)"> Email: </strong></td>
                          <td style="padding:6px;border-bottom: 1px solid rgb(237 237 237);">$from_email </td>
                       </tr>
                      <tr>
                          <td style="padding:6px ; border-bottom: 1px solid rgb(237 237 237); border-right: 1px solid rgb(237 237 237);">
                          <strong style="color: rgb(114 191 68)"> Phone: </strong></td>
                          <td style="padding:6px;border-bottom: 1px solid rgb(237 237 237);">$phone</td>
                     </tr>
                      <tr>
                         <td style="padding:6px ; border-bottom: 1px solid rgb(237 237 237); border-right: 1px solid rgb(237 237 237);">
                         <strong style="color: rgb(114 191 68)"> Company: </strong></td>
                          <td style="padding:6px;border-bottom: 1px solid rgb(237 237 237);">$company</td>
                      </tr>
                      <tr>
                          <td style="padding:6px ; border-bottom: 1px solid rgb(237 237 237); border-right: 1px solid rgb(237 237 237);">
                          <strong style="color: rgb(114 191 68)">Country: </strong></td>
                         <td style="padding:6px;border-bottom: 1px solid rgb(237 237 237);">$country</td>
                      </tr>
                      <tr>
                           <td style="padding:6px ; border-bottom: 1px solid rgb(237 237 237); border-right: 1px solid rgb(237 237 237);">
                           <strong style="color: rgb(114 191 68)">Services: </strong></td>
                           <td style="padding:6px;border-bottom: 1px solid rgb(237 237 237);">$services</td>
                      </tr>
                         <tr>
                           <td style="padding:6px ; border-bottom: 1px solid rgb(237 237 237); border-right: 1px solid rgb(237 237 237);">
                           <strong style="color: rgb(114 191 68)">Subject: </strong></td>
                          <td style="padding:6px;border-bottom: 1px solid rgb(237 237 237);">$subject </td>
                       </tr>
               </table>
            </td>
          </tr>

       </table>";
        $email_body .= '</body></html>';

        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        $headers .= "From: $from_email\n";
        $headers .= "Reply-To: $from_email";

        mail($to_email, $email_subject, $email_body, $headers);

        $response_array['status'] = 'success';
        $response_array['from'] = $from_email;

        echo json_encode($response_array);
        echo json_encode($from_email);
        header($response_array);
        return $from_email;
?>

