import { Injectable } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { HelperService } from "./helper.service";
import { SharedService } from "../shared/services/shared.service";

@Injectable({
  providedIn: 'root'
})
export class TranslateLanguageService {

  public currentLang: string;
  public defaultLang: string;

  constructor(
    private translateService: TranslateService,
    private sharedService: SharedService,
  ) {
    this.translateService.addLangs(['hy', 'en', 'ru']);
    this.translateService.setDefaultLang('hy');
    this.defaultLang = this.translateService.defaultLang;
    this.currentLang = 'hy';
    this.setLanguage();
  }

  setLanguage(lang?: string): void {
    this.sharedService.showLoader$.next(true);
    const language = lang ? lang : 'hy';
    this.translateService.use(language).subscribe(res => {
      // At this point the lang will be loaded
      this.currentLang = language;
      this.sharedService.showLoader$.next(false);
    });
  }

  useLanguage(language: string) {
    this.sharedService.showLoader$.next(true);
    this.translateService.use(language).subscribe(res => {
      // At this point the lang will be loaded
      this.currentLang = language;
      this.sharedService.showLoader$.next(false);
    });
  }

  getTranslation(key: string, ...args): string {
    return this.translateService.instant(key, ...args);
  }

}
