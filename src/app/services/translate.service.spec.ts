import { TestBed } from '@angular/core/testing';

import { TranslateLanguageService } from './translateLanguage.service';

describe('TranslateService', () => {
  let service: TranslateLanguageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TranslateLanguageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
