import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from "rxjs";
import { MessageService } from "primeng/api";

import { TranslateLanguageService } from "./translateLanguage.service";
import { SharedService } from "../shared/services/shared.service";

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  public showPopup$: Subject<boolean> = new Subject<boolean>();
  public showSuccessMessage$: Subject<boolean> = new Subject<boolean>();
  public showErrorMessage$: Subject<boolean> = new Subject<boolean>();
  public url$: BehaviorSubject<string> = new BehaviorSubject<string>('/home');
  showHeader: boolean;

  constructor(
    private messageService: MessageService,
    public translateLanguageService: TranslateLanguageService,
    private sharedService: SharedService
  ) {
    this.showSuccessMessage$.next(false);
    this.showErrorMessage$.next(false);
  }

  public showSuccessMessage(): void {
    this.sharedService.showLoader$.next(false);
    this.messageService.add({
      severity: 'success', summary: 'Success Message',
      detail: this.translateLanguageService.currentLang === 'en' ? 'We received your request successfully. Thank you!': this.translateLanguageService.currentLang === 'hy' ?
        'Ձեր հարցումը հաջողությամբ ստացված է: Շնորհակալություն։' : 'Ваш запрос успешно получен. Спасибо.'
    });
  }

  public showErrorMessage(): void {
    this.sharedService.showLoader$.next(false);
    this.messageService.add({
        severity: 'error', summary: 'Error Message',
        detail: this.translateLanguageService.currentLang === 'en' ? 'Error occurred while sending email. Please try again later.' : this.translateLanguageService.currentLang === 'hy' ?
          'Տեղի ունեցավ սխալ։ Խնդրում ենք քիչ հետո կրկին փորձել' : 'Error occurred while sending email. Please try again later.'
      }
    );

  }

}
