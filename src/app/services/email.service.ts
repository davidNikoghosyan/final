import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {Email} from '../core/interfaces';

@Injectable({
  providedIn: 'root'
})
export class EmailService {

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  public sendEmail(data: Email): Observable<Email> | any {
    return this.httpClient.post('https://itreports.am//assets/send-email/email-client.php', data);
  }

}
