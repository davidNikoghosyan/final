import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from "@angular/router";
import { GoogleTagManagerService } from "angular-google-tag-manager";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { NgwWowService } from "ngx-wow";
import { PrimeNGConfig } from 'primeng/api';

import { HelperService } from "./services/helper.service";
import { TranslateLanguageService } from "./services/translateLanguage.service";
import { SharedService } from "./shared/services/shared.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent implements OnInit, OnDestroy {

  public title = 'ItReports';
  public showPopup: boolean;
  public showPopupButton: boolean;
  private destroy$: Subject<void> = new Subject<void>();
  private currentPosition = document.body.getBoundingClientRect().top;

  constructor(
    public helperService: HelperService,
    public router: Router,
    public sharedService: SharedService,
    private wowService: NgwWowService,
    public translateLanguageService: TranslateLanguageService,
    private primengConfig: PrimeNGConfig,
    private gtmService: GoogleTagManagerService,
  ) {
  }

  @HostListener("window:scroll", [ '$event.target' ])
  addPopupButton(event): void {
    let scroll = event.scrollingElement.scrollTop;
    if (scroll > this.currentPosition) {
      this.showPopupButton = true;
    } else {
      this.showPopupButton = false;
    }
  }

  ngOnInit() {
    this.pushGTMTags();
    if(/^\?fbclid=/.test(location.search)) {
      location.replace(location.href.replace(/\?fbclid.+/, ""));
    }
    this.wowService.init();
    this.showPopupSubscription();
    this.primengConfig.ripple = true;
    this.router.events.forEach(item => {
      if (item instanceof NavigationEnd) {
        const gtmTag = {
          event: 'page',
          pageName: item.url
        };

        this.gtmService.pushTag(gtmTag);
      }
    });
  }

  private pushGTMTags(): void {
    const gtmTags = [
      { companyName: 'ITReports' },
      { serviceCategory: 'Tax service Armenia' },
      { serviceCategory: 'Business Relocation in Armenia' },
      { serviceCategory: 'Accounting Services' },
      { serviceCategory: 'IT Business Relocation' },
      { serviceCategory: 'Legal services in Armenia' },
      { serviceCategory: 'Accounting companies in armenia' },
      { serviceCategory: 'Financial Accounting' },
      { serviceCategory: 'Organization Registration' },
    ];
    gtmTags.forEach(gtmTag => this.gtmService.pushTag(gtmTag))
  }

  private showPopupSubscription(): void {
    this.helperService.showPopup$
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => this.showPopup = res);
  }

  openPopup(event): void {
    event.stopPropagation();
    this.helperService.showPopup$.next(true);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

}
