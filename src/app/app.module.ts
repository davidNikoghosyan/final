import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { MessageService } from "primeng/api";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from "./shared/shared.module";
import { environment } from "../environments/environment";
import { Environment } from "./core/models";
import { GoogleTagManagerModule } from "angular-google-tag-manager";

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SharedModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AppRoutingModule,
    GoogleTagManagerModule.forRoot({
      id: 'G-HQJ6GWFSSK',
    })
  ],
  providers: [
    MessageService,
    {provide: Environment, useValue: environment},
    // {provide: 'googleTagManagerId', useValue: 'G-HQJ6GWFSSK'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
