import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarouselModule } from "ngx-owl-carousel-o";

import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from "./public.component";
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { PartnersComponent } from './partners/partners.component';
import { TeamComponent } from './team/team.component';
import { TestimonialsComponent } from "./testimonials/testimonials.component";
import { SharedModule } from "../shared/shared.module";
import { ContactsComponent } from './contacts/contacts.component';


@NgModule({
  declarations: [
    PublicComponent,
    HomeComponent,
    AboutComponent,
    PartnersComponent,
    TeamComponent,
    TestimonialsComponent,
    ContactsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    PublicRoutingModule,
    CarouselModule
  ]
})
export class PublicModule { }
