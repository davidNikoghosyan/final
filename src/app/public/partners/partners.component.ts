import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { NgwWowService } from "ngx-wow";
import { OwlOptions } from "ngx-owl-carousel-o";

import { TranslateLanguageService } from "../../services/translateLanguage.service";
import { CheckHeader } from "../../core/functions";
import { Environment } from "../../core/models";
import { HelperService } from "../../services/helper.service";

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.scss']
})
export class PartnersComponent implements OnInit {

  public baseURL = this.environment.webUrl;
  public showHeader = false;
  public selectedItem = 1;
  public partners = [];
  public responsiveOptions: Array<any> = [];
  public options: OwlOptions = {};
  public navBarData: Array<any> = [];

  constructor(
    private router: Router,
    private wowService: NgwWowService,
    public translateLanguageService: TranslateLanguageService,
    public helperService: HelperService,
    private environment: Environment
  ) { }

  ngOnInit(): void {
    this.helperService.url$.next('/partners');
    this.showHeader = CheckHeader(this.router.url.split('?')[0]);
    this.setOptions();
    this.setNavBarData();
  }

  private setOptions(): void {
    this.options = {
      loop: true,
      autoplay: true,
      autoplayHoverPause: true,
      dots: true,
      touchDrag: true,
      autoHeight: true,
      autoWidth: true,
      nav: true,
      navSpeed: 800,
      autoplaySpeed: 3000,
      margin: 30,
      responsive: {
        560: {
          items: 1
        },
        768: {
          items: 2,
        },
        1024: {
          items: 4
        }
      },
    };
    this.responsiveOptions = [
      {
        breakpoint: '0',
        numVisible: 1,
        numScroll: 1
      },
      {
        breakpoint: '768px',
        numVisible: 2,
        numScroll: 2
      },
      {
        breakpoint: '1200',
        numVisible: 3,
        numScroll: 3
      }

    ];
    this.partners = [
      {
        id: 1,
        link: 'https://floopen.com',
        src: "/assets/images/partners/img-1.png",
      },
      {
        id: 2,
        link: 'https://hgdevs.com/?fbclid=IwAR1CM2W6PCfmg1oThX9iS4Pwp39iXJrYP1JQt5OWlPV9Bwm7aNNyfFoeB5Q',
        src: "/assets/images/partners/img-2.png",
      },
      {
        id: 3,
        link: 'https://egypt.anrivatour.am/',
        src: "/assets/images/partners/img-3.png",
      },
      {
        id: 4,
        link: 'https://www.fambox.pro/main',
        src: "/assets/images/partners/img-4.png",
      },
      {
        id: 5,
        link: 'https://krit.pro/en/',
        src: "/assets/images/partners/img-5.png",
      },
      {
        id: 6,
        link: 'https://leanzer.com/',
        src: "/assets/images/partners/img-6.png",
      },
      {
        id: 7,
        link: 'https://applebrie.com/',
        src: "/assets/images/partners/img-7.png",
      },
      {
        id: 8,
        link: 'https://fwtech.am/',
        src: "/assets/images/partners/img-8.png",
      },
      {
        id: 9,
        link: 'https://sapiens.solutions/en',
        src: "/assets/images/partners/img-9.png",
      },
      {
        id: 10,
        link: 'https://aimtech.am/',
        src: "/assets/images/partners/img-10.png",
      },
      {
        id: 11,
        link: 'https://alexhome.am/',
        src: "/assets/images/partners/img-11.png",
      },
      {
        id: 12,
        link: 'https://www.facebook.com/CoinSoft.Armenia/',
        src: "/assets/images/partners/img-12.png",
      },
      {
        id: 13,
        link: 'https://urbamatica.com/',
        src: "/assets/images/partners/img-13.png",
      },
      {
        id: 14,
        link: 'https://www.mic.am/',
        src: "/assets/images/partners/img-14.png",
      },
      {
        id: 15,
        link: 'https://www.netris.io/',
        src: "/assets/images/partners/img-15.png",
      },
      {
        id: 16,
        link: 'https://www.parzlogic.com/',
        src: "/assets/images/partners/img-16.png",
      },
      {
        id: 17,
        link: 'https://www.sololearn.com',
        src: "/assets/images/partners/img-17.png",
      },
      {
        id: 18,
        link: 'https://www.tidepoollabs.com/',
        src: "/assets/images/partners/img-18.png",
      },
      {
        id: 19,
        link: 'https://www.vosesoftware.com/',
        src: "/assets/images/partners/img-19.png",
      },
      {
        id: 20,
        link: 'https://zorotime.ru/',
        src: "/assets/images/partners/img-20.png",
      },
      {
        id: 21,
        link: 'https://dirion.ru/',
        src: "/assets/images/partners/img-21.png",
      },
      {
        id: 22,
        link: 'http://modulepro.online/',
        src: "/assets/images/partners/img-22.png",
      },
      {
        id: 23,
        link: 'https://www.instagram.com/arusik_tiramisu/',
        src: "/assets/images/partners/img-23.png",
      },
      {
        id: 24,
        link: 'http://officetechnologies.ge/',
        src: "/assets/images/partners/img-24.png",
      },
      {
        id: 25,
        link: 'https://aexsoft.ru/',
        src: "/assets/images/partners/img-25.png",
      },
      {
        id: 26,
        link: 'https://arintellect.am/',
        src: "/assets/images/partners/img-26.png",
      },
      {
        id: 27,
        link: 'https://bookprize.am/',
        src: "/assets/images/partners/img-27.png",
      },
      {
        id: 28,
        link: 'https://swiftech.am/',
        src: "/assets/images/partners/img-28.png",
      },
      {
        id: 29,
        link: 'http://softbridge.am/',
        src: "/assets/images/partners/img-29.png",
      },
      {
        id: 30,
        link: 'http://applebrie.com/',
        src: "/assets/images/partners/img-30.png",
      },
      {
        id: 31,
        link: 'https://fr.shop-orchestra.com/',
        src: "/assets/images/partners/img-31.png",
      },
      {
        id: 32,
        link: 'https://koreez.games/',
        src: "/assets/images/partners/img-32.png",
      },
      {
        id: 33,
        link: 'http://www.pulsarlogic.com/',
        src: "/assets/images/partners/img-33.png",
      },
      {
        id: 34,
        link: 'https://www.redbridge.am/',
        src: "/assets/images/partners/img-34.png",
      },
      // {
      //   id: 35,
      //   link: 'https://www.coderepublic.am',
      //   src: "/assets/images/partners/img-35.svg",
      // },
      {
        id: 36,
        link: 'https://justexpert.am/',
        src: "/assets/images/partners/img-36.png",
      },
      {
        id: 37,
        link: 'https://www.facebook.com/elitshinanyutaslanyanner/',
        src: "/assets/images/partners/img-37.png",
      },
      {
        id: 38,
        link: 'https://ard-trading.am/',
        src: "/assets/images/partners/img-38.png",
      },
    ];
  }

  private setNavBarData(): void {
    this.navBarData = [
      {
        index: 1,
        title: 'NavBarData.Title2',
        src: '/assets/images/values/img-2.jpg',
        contentTitle: 'NavBarData.ContentTitle2',
        content: ['NavBarData.Content21', 'NavBarData.Content22', 'NavBarData.Content23', 'NavBarData.Content24', 'NavBarData.Content25']

      },
      {
        index: 2,
        title: 'NavBarData.Title3',
        src: '/assets/images/values/img-3.jpg',
        contentTitle: 'NavBarData.ContentTitle3',
        content: ['NavBarData.Content31', 'NavBarData.Content32', 'NavBarData.Content33',
          'NavBarData.Content34', 'NavBarData.Content35', 'NavBarData.Content36', 'NavBarData.Content37', 'NavBarData.Content38']
      }
    ]
  }

  changeContent(event, index: number): void {
    event.stopPropagation();
    this.wowService.init();
    this.selectedItem = index;
  }

}
