import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { NgwWowService } from "ngx-wow";

import { TranslateLanguageService } from "../../services/translateLanguage.service";
import { Environment } from "../../core/models";
import { CheckHeader } from "../../core/functions";
import { HelperService } from "../../services/helper.service";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  public baseURL = this.environment.webUrl;
  public showHeader = false;
  public selectedItem = 1;
  public navBarData: Array<any> = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private wowService: NgwWowService,
    public translateLanguageService: TranslateLanguageService,
    public helperService: HelperService,
    private environment: Environment
  ) {
  }

  ngOnInit(): void {
    this.helperService.url$.next('/about');
    this.showHeader = CheckHeader(this.router.url.split('?')[0]);
    this.setNavBarData();
  }

  private setNavBarData(): void {
    this.navBarData = [
      {
        index: 1,
        title: 'NavBarData.Title2',
        src: '/assets/images/values/img-2.jpg',
        contentTitle: 'NavBarData.ContentTitle2',
        content: ['NavBarData.Content21', 'NavBarData.Content22', 'NavBarData.Content23', 'NavBarData.Content24', 'NavBarData.Content25',
          'NavBarData.Content26', 'NavBarData.Content27', 'NavBarData.Content28']

      },
      {
        index: 2,
        title: 'NavBarData.Title3',
        src: '/assets/images/values/img-3.jpg',
        contentTitle: 'NavBarData.ContentTitle3',
        content: ['NavBarData.Content31', 'NavBarData.Content32', 'NavBarData.Content33',
          'NavBarData.Content34', 'NavBarData.Content35', 'NavBarData.Content36', 'NavBarData.Content37', 'NavBarData.Content38']
      }
    ]
  }

  changeContent(event, index: number): void {
    event.stopPropagation();
    this.wowService.init();
    this.selectedItem = index;
  }

}
