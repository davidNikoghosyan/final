import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { CheckHeader } from "../../core/functions";
import { Environment } from "../../core/models";
import { HelperService } from "../../services/helper.service";

@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {

  public baseURL = this.environment.webUrl;
  public showHeader = false;
  public teamMembersData = [];

  constructor(
    private router: Router,
    private environment: Environment,
    private helperService: HelperService,
  ) { }

  ngOnInit(): void {
    this.showHeader = CheckHeader(this.router.url.split('?')[0]);
    this.setTeamMembersData();
  }

  setTeamMembersData(): void {
    this.teamMembersData = [
      {headerTitle: 'Team.Member1.Title', src: '/assets/images/team/img-1.jpg', description: 'Team.Member1.Description'},
      {headerTitle: 'Team.Member2.Title', src: '/assets/images/team/img-2.jpg', description: 'Team.Member2.Description'},
      {headerTitle: 'Team.Member3.Title', src: '/assets/images/team/img-3.jpg', description: 'Team.Member3.Description'},
      {headerTitle: 'Team.Member4.Title', src: '/assets/images/team/img-4.jpg', description: 'Team.Member4.Description'},
      {headerTitle: 'Team.Member5.Title', src: '/assets/images/team/img-5.jpg', description: 'Team.Member5.Description'},
      {headerTitle: 'Team.Member6.Title', src: '/assets/images/team/img-6.jpg', description: 'Team.Member6.Description'},
      {headerTitle: 'Team.Member7.Title', src: '/assets/images/team/img-7.jpg', description: 'Team.Member7.Description'},
      {headerTitle: 'Team.Member8.Title', src: '/assets/images/team/img-8.jpg', description: 'Team.Member8.Description'},
      {headerTitle: 'Team.Member10.Title', src: '/assets/images/team/img-10.jpg', description: 'Team.Member10.Description'},
      {headerTitle: 'Team.Member11.Title', src: '/assets/images/team/img-11.png', description: 'Team.Member11.Description'},
      {headerTitle: 'Team.Member12.Title', src: '/assets/images/team/img-12.png', description: 'Team.Member12.Description'},
      {headerTitle: 'Team.Member13.Title', src: '/assets/images/team/img-13.jpg', description: 'Team.Member13.Description'},
      {headerTitle: 'Team.Member14.Title', src: '/assets/images/team/img-14.png', description: 'Team.Member14.Description'},
      {headerTitle: 'Team.Member15.Title', src: '/assets/images/team/img-15.png', description: 'Team.Member15.Description'},
      {headerTitle: 'Team.Member16.Title', src: '/assets/images/team/img-16.png', description: 'Team.Member16.Description'},
      {headerTitle: 'Team.Member17.Title', src: '/assets/images/team/img-17.jpg', description: 'Team.Member17.Description'},
      {headerTitle: 'Team.Member18.Title', src: '/assets/images/team/img-18.jpg', description: 'Team.Member18.Description'},
    ];
  }

}
