import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import { TranslateLanguageService } from "../../services/translateLanguage.service";
import { HelperService } from "../../services/helper.service";
import { CheckHeader } from "../../core/functions";

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {

  showHeader: boolean;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private translateLanguageService: TranslateLanguageService,
    private helperService: HelperService,
  ) { }

  ngOnInit(): void {
    this.helperService.url$.next('/contacts');
    this.showHeader = CheckHeader(this.router.url.split('?')[0]);
  }

  public openPopup(event): void {
    event.stopPropagation();
    this.helperService.showPopup$.next(true);
  }

}
