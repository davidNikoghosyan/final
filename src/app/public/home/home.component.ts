import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { NgwWowService } from "ngx-wow";

import { HelperService } from "../../services/helper.service";
import { Environment } from "../../core/models";
import { TranslateLanguageService } from "../../services/translateLanguage.service";
import { CheckHeader } from "../../core/functions";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.scss' ]
})
export class HomeComponent implements OnInit, OnDestroy {

  public baseURL = this.environment.webUrl;
  public showHeader = false;
  public showPopup: boolean;
  public mode = '';
  public selectedItem = 1;
  public slideImages = [
    { id: 1, path: '/assets/images/slider/img1.jpg', title: 'Header.Slide.Title1'},
    { id: 2, path: '/assets/images/slider/img2.jpg', title: 'Header.Slide.Title2'},
    { id: 3, path: '/assets/images/slider/img3.jpg', title: 'Header.Slide.Title3'},
  ];
  public navBarData: Array<any> = [];
  public responsiveOptions: any[] = [
    {
      breakpoint: '1024px',
      numVisible: 2
    },
    {
      breakpoint: '768px',
      numVisible: 1
    },
    {
      breakpoint: '560px',
      numVisible: 1
    }
  ];
  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public translateLanguageService: TranslateLanguageService,
    private wowService: NgwWowService,
    private helperService: HelperService,
    private environment: Environment
  ) {
  }

  ngOnInit(): void {
    this.helperService.url$.next('/home');
    this.showHeader = CheckHeader('/home');
    this.showPopupSubscription();
    this.setNavBarData();
  }

  private setNavBarData(): void {
    this.navBarData = [
      {
        index: 1,
        title: 'NavBarData.Title2',
        src: '/assets/images/values/img-2.jpg',
        contentTitle: 'NavBarData.ContentTitle2',
        content: ['NavBarData.Content21', 'NavBarData.Content22', 'NavBarData.Content23', 'NavBarData.Content24', 'NavBarData.Content25',
          'NavBarData.Content26', 'NavBarData.Content27', 'NavBarData.Content28']

      },
      {
        index: 2,
        title: 'NavBarData.Title3',
        src: '/assets/images/values/img-3.jpg',
        contentTitle: 'NavBarData.ContentTitle3',
        content: ['NavBarData.Content31', 'NavBarData.Content32', 'NavBarData.Content33',
          'NavBarData.Content34', 'NavBarData.Content35', 'NavBarData.Content36', 'NavBarData.Content37', 'NavBarData.Content38']
      }
    ]
  }

  changeContent(event, index: number): void {
    event.stopPropagation();
    this.wowService.init();
    this.selectedItem = index;
  }


  private showPopupSubscription(): void {
    this.helperService.showPopup$
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => this.showPopup = res);
  }

  public openPopup(event): void {
    event.stopPropagation();
    this.helperService.showPopup$.next(true);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

}
