import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxAccountingComponent } from './tax-accounting.component';

describe('TaxAccountingComponent', () => {
  let component: TaxAccountingComponent;
  let fixture: ComponentFixture<TaxAccountingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaxAccountingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxAccountingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
