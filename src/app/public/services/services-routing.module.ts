import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ServicesComponent } from "./services.component";
import { TaxAccountingComponent } from "./tax-accounting/tax-accounting.component";
import { StartupConsultingComponent } from "./startup-consulting/startup-consulting.component";
import { OtherRelatedComponent } from "./other-related/other-related.component";
import { FinancialAccountingComponent } from "./finansical-accounting/financial-accounting.component";
import { HrManagementComponent } from "./hr-management/hr-management.component";
import { LegalServicesComponent } from "./legal-services/legal-services.component";
import { RelocationComponent } from "./relocation/relocation.component";
import { ServicesViewComponent } from "./services-view/services-view.component";

const routes: Routes = [
  {
    path: '', component: ServicesComponent,
    children: [
      {path: '', component: ServicesViewComponent, pathMatch: 'full'},
      {path: 'tax-accounting', component: TaxAccountingComponent},
      {path: 'financial-accounting', component: FinancialAccountingComponent},
      {path: 'startup-consulting', component: StartupConsultingComponent},
      {path: 'legal-services', component: LegalServicesComponent},
      {path: 'relocation', component: RelocationComponent},
      {path: 'HR-management', component: HrManagementComponent},
      {path: 'other', component: OtherRelatedComponent},
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ServicesRoutingModule { }
