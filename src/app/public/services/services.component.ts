import { Component, OnInit } from '@angular/core';
import { HelperService } from "../../services/helper.service";

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.sass']
})
export class ServicesComponent implements OnInit {

  constructor(
    public helperService: HelperService,
  ) { }

  ngOnInit(): void {
    this.helperService.url$.next('/services');
  }

}
