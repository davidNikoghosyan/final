import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServicesRoutingModule } from './services-routing.module';
import { ServicesComponent } from "./services.component";
import { TaxAccountingComponent } from "./tax-accounting/tax-accounting.component";
import { OtherRelatedComponent } from "./other-related/other-related.component";
import { FinancialAccountingComponent } from "./finansical-accounting/financial-accounting.component";
import { StartupConsultingComponent } from "./startup-consulting/startup-consulting.component";
import { HrManagementComponent } from "./hr-management/hr-management.component";
import { SharedModule } from "../../shared/shared.module";
import { RelocationComponent } from './relocation/relocation.component';
import { LegalServicesComponent } from './legal-services/legal-services.component';
import { ServicesViewComponent } from './services-view/services-view.component';


@NgModule({
  declarations: [
    ServicesComponent,
    HrManagementComponent,
    TaxAccountingComponent,
    FinancialAccountingComponent,
    OtherRelatedComponent,
    StartupConsultingComponent,
    RelocationComponent,
    LegalServicesComponent,
    ServicesViewComponent,
  ],
  exports: [
    ServicesComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ServicesRoutingModule
  ]
})
export class ServicesModule { }
