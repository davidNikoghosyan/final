import { Component, OnInit } from '@angular/core';
import { TranslateLanguageService } from "../../../services/translateLanguage.service";

@Component({
  selector: 'app-services-view',
  templateUrl: './services-view.component.html',
  styleUrls: ['./services-view.component.scss']
})
export class ServicesViewComponent implements OnInit {

  constructor(
    public translateLanguageService: TranslateLanguageService,
  ) { }

  ngOnInit(): void {
  }

}
