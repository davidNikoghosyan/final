import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { TranslateLanguageService } from "../../../services/translateLanguage.service";
import { CheckHeader } from "../../../core/functions";
import { Environment } from "../../../core/models";

@Component({
  selector: 'app-other-related',
  templateUrl: './other-related.component.html',
  styleUrls: ['./other-related.component.sass']
})
export class OtherRelatedComponent implements OnInit {

  public baseURL = this.environment.webUrl;
  public showHeader = false;

  constructor(
    private router: Router,
    public translateLanguageService: TranslateLanguageService,
    private environment: Environment
  ) { }

  ngOnInit(): void {
    this.showHeader = CheckHeader('/services');
  }

}
