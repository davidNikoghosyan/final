import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { TranslateLanguageService } from "../../../services/translateLanguage.service";
import { Environment } from "../../../core/models";
import { CheckHeader } from "../../../core/functions";

@Component({
  selector: 'app-legal-services',
  templateUrl: './legal-services.component.html',
  styleUrls: ['./legal-services.component.scss']
})
export class LegalServicesComponent implements OnInit {

  public baseURL = this.environment.webUrl;
  public showHeader = false;

  constructor(
    private router: Router,
    public translateLanguageService: TranslateLanguageService,
    private environment: Environment
  ) { }

  ngOnInit(): void {
    this.showHeader = CheckHeader('/services');
  }

}
