import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { OwlOptions } from "ngx-owl-carousel-o";

import { CheckHeader } from "../../core/functions";
import { Environment } from "../../core/models";

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.scss']
})
export class TestimonialsComponent implements OnInit {

  public baseURL = this.environment.webUrl;
  public showHeader = false;
  public testimonials = [];
  public options: OwlOptions = {};
  responsiveOptions: Array<any> = [];

  constructor(
    private router: Router,
    private environment: Environment
  ) { }

  ngOnInit(): void {
    this.showHeader = CheckHeader(this.router.url.split('?')[0]);
    this.setOptions();
  }

  public readMore(id: number): void {
    this.testimonials.map(el => {
      if (el.id === id) {
        el.showReadMoreButton = !el.showReadMoreButton;
        el.showMoreData = !el.showMoreData;
      }
      return el;
    });
  }

  private setOptions(): void {
    this.options = {
      loop: true,
      autoplay: true,
      autoplayHoverPause: true,
      touchDrag: true,
      dots: false,
      autoHeight: true,
      nav: true,
      autoWidth: true,
      navSpeed: 800,
      autoplaySpeed: 3000,
      margin: 30,
      responsive: {
        0: {
          items: 1,
        },
        768: {
          items: 2,
        },
        1200: {
          items: 3
        }
      },
    };
    this.responsiveOptions = [
      {
        breakpoint: '1200px',
        numVisible: 2,
        numScroll: 2
      },
      // {
      //   breakpoint: '991px',
      //   numVisible: 2,
      //   numScroll: 2
      // },
      {
        breakpoint: '800px',
        numVisible: 1,
        numScroll: 1
      }
    ];
    this.testimonials = [
      {
        id: 1,
        src: '/assets/images/testimonials/img-1.png',
        name: 'Testimonials.PartnersName1',
        position: 'Testimonials.PartnersDescription1',
        description: 'Testimonials.PartnersContent1',
        showMoreData: false,
        showReadMoreButton: false,
      },
      {
        id: 2,
        src: '/assets/images/testimonials/img-2.png',
        name: 'Testimonials.PartnersName2',
        position: 'Testimonials.PartnersDescription2',
        description: "Testimonials.PartnersContent2",
        showMoreData: false,
        showReadMoreButton: false,
      },
      {
        id: 3,
        src: '/assets/images/testimonials/img-3.png',
        name: 'Testimonials.PartnersName3',
        position: 'Testimonials.PartnersDescription3',
        description: "Testimonials.PartnersContent3",
        moreDescription: "Testimonials.PartnersContent3",
        showMoreData: false,
        showReadMoreButton: true,
      },
      {
        id: 4,
        src: '/assets/images/testimonials/img-4.png',
        name: 'Testimonials.PartnersName4',
        position: 'Testimonials.PartnersDescription4',
        description: "Testimonials.PartnersContent4",
        moreDescription: "Testimonials.PartnersContent4",
        showMoreData: false,
        showReadMoreButton: true,
      },
      {
        id: 5,
        src: '/assets/images/testimonials/img-5.png',
        name: 'Testimonials.PartnersName5',
        position: 'Testimonials.PartnersDescription5',
        description: "Testimonials.PartnersContent5",
        moreDescription: "Testimonials.PartnersContent5",
        showMoreData: false,
        showReadMoreButton: true,
      },
      {
        id: 6,
        src: '/assets/images/testimonials/img-6.png',
        name: 'Testimonials.PartnersName6',
        position: 'Testimonials.PartnersDescription6',
        description: "Testimonials.PartnersContent61",
        moreDescription: "Testimonials.PartnersContent62",
        showMoreData: false,
        showReadMoreButton: true,
      },
      {
        id: 7,
        src: '/assets/images/testimonials/img-7.png',
        name: 'Testimonials.PartnersName7',
        position: 'Testimonials.PartnersDescription7',
        description: "Testimonials.PartnersContent7",
        moreDescription: "Testimonials.PartnersContent7",
        showMoreData: false,
        showReadMoreButton: true,
      },
      {
        id: 8,
        src: '/assets/images/testimonials/img-8.png',
        name: 'Testimonials.PartnersName8',
        position: 'Testimonials.PartnersDescription8',
        description: "Testimonials.PartnersContent81",
        moreDescription: "Testimonials.PartnersContent82",
        showMoreData: false,
        showReadMoreButton: true,
      },
      {
        id: 9,
        src: '/assets/images/testimonials/img-9.png',
        name: 'Testimonials.PartnersName9',
        position: 'Testimonials.PartnersDescription9',
        description: "Testimonials.PartnersContent9",
        moreDescription: "Testimonials.PartnersContent9",
        showMoreData: false,
        showReadMoreButton: true,
      },
      {
        id: 10,
        src: '/assets/images/testimonials/img-10.jpeg',
        name: 'Testimonials.PartnersName10',
        position: 'Testimonials.PartnersDescription10',
        description: "Testimonials.PartnersContent10",
        moreDescription: "Testimonials.PartnersContent10",
        showMoreData: false,
        showReadMoreButton: true,
      },
    ];
  }

}
