import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PublicComponent } from "./public.component";
import { HomeComponent } from "./home/home.component";
import { AboutComponent } from "./about/about.component";
import { PartnersComponent } from "./partners/partners.component";
import { ContactsComponent } from "./contacts/contacts.component";

const routes: Routes = [
  {path: '', component: PublicComponent,
    children: [
      {path: '', redirectTo: 'home', pathMatch: 'full'},
      {path: 'home', component: HomeComponent},
      {path: 'about-us', component: AboutComponent},
      {path: 'blog',
        loadChildren: () => import('./blog/blogs.module').then(m => m.BlogsModule),
      },
      {
        path: 'services',
        loadChildren: () => import('./services/services.module').then(m => m.ServicesModule),
      },
      {
        path: 'partners', component: PartnersComponent
      },
      {
        path: 'contacts', component: ContactsComponent
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
