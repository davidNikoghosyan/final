import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogsRoutingModule } from './blogs-routing.module';
import { BlogComponent } from "./blog/blog.component";
import { BlogDetailsComponent } from "./blog-details/blog-details.component";
import { LeftSidebarComponent } from "./left-sidebar/left-sidebar.component";
import { BlogsComponent } from "./blogs.component";
import { SharedModule } from "../../shared/shared.module";


@NgModule({
  declarations: [
    BlogDetailsComponent,
    LeftSidebarComponent,
    BlogsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    BlogsRoutingModule
  ],
  exports: [
    BlogsComponent,
    LeftSidebarComponent,
    BlogDetailsComponent,
  ]
})
export class BlogsModule { }
