import { Component, OnInit } from '@angular/core';
import { Environment } from "../../../core/models";

@Component({
  selector: 'app-left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.sass']
})
export class LeftSidebarComponent implements OnInit {

  public baseURL = this.environment.webUrl;
  public showArchive: boolean;

  constructor(
    private environment: Environment
  ) { }

  ngOnInit(): void {
  }

  public onClick(): void {
    this.showArchive = !this.showArchive;
  }

}
