import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { CheckHeader } from "../../core/functions";
import { TranslateLanguageService } from "../../services/translateLanguage.service";
import { Environment } from "../../core/models";

@Component({
  selector: 'app-blogs',
  templateUrl: './blogs.component.html',
  styleUrls: ['./blogs.component.sass']
})
export class BlogsComponent implements OnInit {

  public baseURL = this.environment.webUrl;
  public showHeader = false;

  constructor(
    private router: Router,
    public translateLanguageService: TranslateLanguageService,
    private environment: Environment
  ) { }

  ngOnInit(): void {
    this.showHeader = CheckHeader(this.router.url.split('?')[0]);
  }

}
