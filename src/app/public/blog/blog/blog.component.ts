import { Component, Input, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { CheckHeader } from "../../../core/functions";
import { Environment } from "../../../core/models";
import { TranslateLanguageService } from "../../../services/translateLanguage.service";

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.sass']
})
export class BlogComponent implements OnInit {

  public baseURL = this.environment.webUrl;
  public showHeader = false;
  @Input() showArchive = true;
  public blogs = [];

  constructor(
    private router: Router,
    public translateLanguageService: TranslateLanguageService,
    private environment: Environment
  ) { }

  ngOnInit(): void {
    this.showHeader = CheckHeader(this.router.url.split('?')[0]);
    this.blogs = [];
  }

}
