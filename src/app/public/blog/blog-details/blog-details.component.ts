import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Environment } from "../../../core/models";

@Component({
  selector: 'app-blog-details',
  templateUrl: './blog-details.component.html',
  styleUrls: ['./blog-details.component.sass']
})
export class BlogDetailsComponent implements OnInit {

  public baseURL = this.environment.webUrl;
  public contentIndex: number;

  constructor(
    private router: Router,
    private environment: Environment,
  ) { }

  ngOnInit(): void {
    this.initBlogDetailsContent();
  }

  private initBlogDetailsContent(): void {
    const routerUrl = this.router.url.split('/');
    this.contentIndex = +routerUrl[routerUrl.length - 1];
  }

}
