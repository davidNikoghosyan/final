import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from '@angular/router';
import { MenubarModule } from "primeng/menubar";
import { MenuModule } from "primeng/menu";
import { DropdownModule } from "primeng/dropdown";

import { FooterComponent } from "./components/footer/footer.component";
import { PageNotFoundComponent } from "./components/page-not-found/page-not-found.component";
import { HeaderComponent } from "./components/header/header.component";
import { RequestComponent } from "./popups/request/request.component";
import { BlogComponent } from "../public/blog/blog/blog.component";
import { ScrollPanelModule } from "primeng/scrollpanel";
import { DialogModule } from "primeng/dialog";
import { PaginatorModule } from "primeng/paginator";
import { ToastModule } from "primeng/toast";
import { NgwWowModule } from "ngx-wow";
import { TranslateModule } from "@ngx-translate/core";
import { GalleriaModule } from "primeng/galleria";
import { CarouselModule } from "primeng/carousel";
import { CardModule } from "primeng/card";
import { ScrollTopModule } from "primeng/scrolltop";
import { ProgressSpinnerModule } from "primeng/progressspinner";
import { RippleModule } from "primeng/ripple";
import { GMapModule } from "primeng/gmap";
import { MultiSelectModule } from "primeng/multiselect";
import { GMapComponent } from './components/gmap/gmap.component';
import { DividerModule } from "primeng/divider";
import { TabMenuModule } from "primeng/tabmenu";



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    BlogComponent,
    RequestComponent,
    GMapComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    TranslateModule.forChild(),
    MenubarModule,
    MenuModule,
    DropdownModule,
    MultiSelectModule,
    ToastModule,
    DialogModule,
    PaginatorModule,
    ScrollPanelModule,
    GalleriaModule,
    CarouselModule,
    CardModule,
    ScrollTopModule,
    ProgressSpinnerModule,
    RippleModule,
    GMapModule,
    DividerModule,
    TabMenuModule,
    NgwWowModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    PageNotFoundComponent,
    BlogComponent,
    RequestComponent,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    MenubarModule,
    MenuModule,
    DropdownModule,
    MultiSelectModule,
    ToastModule,
    DialogModule,
    PaginatorModule,
    ScrollPanelModule,
    NgwWowModule,
    GalleriaModule,
    CarouselModule,
    CardModule,
    ScrollTopModule,
    ProgressSpinnerModule,
    RippleModule,
    GMapModule,
    DividerModule,
    TabMenuModule,
    TranslateModule,
    GMapComponent
  ]
})
export class SharedModule { }
