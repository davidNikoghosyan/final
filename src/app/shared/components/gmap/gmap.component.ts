import { Component, OnInit } from '@angular/core';

declare var google: any;

@Component({
  selector: 'app-gmap',
  templateUrl: './gmap.component.html',
  styleUrls: ['./gmap.component.scss']
})
export class GMapComponent implements OnInit {

  public options: any;
  overlays: any[];

  constructor() { }

  ngOnInit(): void {
    this.setOptions();
  }

  private setOptions(): void {
    this.options = {
      center: {lat: 40.19420, lng: 44.49412},
      zoom: 12
    };
    this.overlays = [
      new google.maps.Marker({position: {lat: 40.19420, lng: 44.49412}, title:"ITReports"}),
    ]
  }

}
