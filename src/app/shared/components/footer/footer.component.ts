import { Component, ElementRef, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { TranslateLanguageService } from "../../../services/translateLanguage.service";
import { Environment } from "../../../core/models";
import { HelperService } from "../../../services/helper.service";
import { SharedService } from "../../services/shared.service";

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public map: any;
  public baseURL = this.environment.webUrl;
  public showHeader = false;
  public url = '';

  constructor(
    private router: Router,
    public translateLanguageService: TranslateLanguageService,
    public el: ElementRef,
    private helperService: HelperService,
    private sharedService: SharedService,
    private environment: Environment
  ) { }

  ngOnInit(): void {
    this.getUrl();
  }

  private getUrl(): void {
    this.helperService.url$.subscribe(res => {
      this.url = res;
    })
  }

}
