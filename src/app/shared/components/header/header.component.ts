import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Params, Router } from "@angular/router";
import { MenuItem } from "primeng/api";

import { TranslateLanguageService } from "../../../services/translateLanguage.service";
import { Environment } from "../../../core/models";
import { LanguageNames } from "../../../core/enums/languages.enum";
import { filter, takeUntil } from "rxjs/operators";
import { Subject } from "rxjs";
import { SharedService } from "../../services/shared.service";
import { Menubar } from "primeng/menubar";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: [ './header.component.scss' ]
})
export class HeaderComponent implements OnInit {

  @ViewChild('sss', { static: false }) sss: Menubar;
  public languages = [
    { value: 'hy', name: 'Arm', path: 'arm.svg' },
    { value: 'en', name: 'Eng', path: 'eng.svg' }
  ];
  selectedLanguage = '';
  public baseURL = this.environment.webUrl;
  public items: MenuItem[] = [];
  public params: Params;
  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    public translateLanguageService: TranslateLanguageService,
    private sharedService: SharedService,
    private environment: Environment
  ) {
    router.events
      .pipe(
        takeUntil(this.destroy$),
        filter(event => event instanceof NavigationEnd))
      .subscribe((event: NavigationEnd) => {
        const url = event.urlAfterRedirects;
        const language = url.split('=')[ 1 ];
        this.setLanguage(language);
        this.setParams();
        this.setMenuItems(this.selectedLanguage);
        this.sharedService.showLoader$.next(false);
      });
  }

  ngOnInit(): void {
  }

  private setParams(): void {
    this.params = {
      queryParams: {
        lang: this.selectedLanguage ? this.selectedLanguage : this.translateLanguageService.currentLang || 'hy'
      }
    };
  }

  private setLanguage(language: string): void {
    language = language ? language : this.translateLanguageService.currentLang || 'hy';
    this.translateLanguageService.useLanguage(language);
    this.selectedLanguage = language;
  }

  private setMenuItems(lang: string): void {
    this.items = [
      {
        label: lang === LanguageNames[ '1_' ] ? 'Մենք' : lang === LanguageNames[ '2_' ] ? 'About us' : 'О нас',
        command: () => this.navigate('/about-us'),
      },
      {
        label: lang === LanguageNames[ '1_' ] ? 'Ծառայություններ' : lang === LanguageNames[ '2_' ] ? 'Services' : 'Сервисы',
        items: [
          {
            label: lang === LanguageNames[ '1_' ] ? 'Հաշվապահական և հարկային հաշվառում' : lang === LanguageNames[ '2_' ] ? 'Financial and Tax accounting' : 'Ведение бухгалтерского и налогового учета',
            command: () => this.navigate('/services/tax-accounting'),

          },
          {
            label: lang === LanguageNames[ '1_' ] ? 'Իրավաբանական ծառայություններ' : lang === LanguageNames[ '2_' ] ? 'Delivery of legal services' : 'Юридические услуги',
            command: () => this.navigate('/services/legal-services'),

          },
          {
            label: lang === LanguageNames[ '1_' ] ? 'Բիզնես խորհրդատվություն' : lang === LanguageNames[ '2_' ] ? 'Business consulting' : 'Бизнес-консалтинг',
            command: () => this.navigate('/services/startup-consulting'),
          },
          {
            label: lang === LanguageNames[ '1_' ] ? 'Բիզնեսի ռելոկացիա' : lang === LanguageNames[ '2_' ] ? 'Business relocation' : 'Релокация бизнеса',
            command: () => this.navigate('/services/relocation'),
          },
        ]
      },
      {
        label: lang === LanguageNames[ '1_' ] ? 'Գործընկեր' : lang === LanguageNames[ '2_' ] ? 'Partners' : 'Партнеры',
        command: () => this.navigate('/partners'),
      },
      {
        label: lang === LanguageNames[ '1_' ] ? 'Կապ' : lang === LanguageNames[ '2_' ] ? 'Contacts' : 'Контакты',
        command: () => this.navigate('/contacts'),
      },
      {
        label: '',
        icon: this.selectedLanguage === 'hy' ? 'lang arm' : this.selectedLanguage === 'en' ? 'lang eng' : 'lang rus',
        items: [
          {
            label: '',
            icon: 'lang arm',
            command: () => this.changeLanguage('hy'),
          },
          {
            label: '',
            icon: 'lang eng',
            command: () => this.changeLanguage('en'),
          },
          {
            label: '',
            icon: 'lang rus',
            command: () => this.changeLanguage('ru'),
          }
        ]
      },
    ];
  }

  changeLanguage(lang: string): void {
    const url = this.router.url.split('?')[ 0 ];
    this.setLanguage(lang);
    this.setParams();
    this.navigate(url);
  }

  navigate(path: string): void {
    this.router.navigate([ path ], this.params);
  }

}
