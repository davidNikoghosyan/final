import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { UntypedFormBuilder, UntypedFormGroup, Validators } from "@angular/forms";

import { TranslateLanguageService } from "../../../services/translateLanguage.service";
import { HelperService } from "../../../services/helper.service";
import { EmailService } from "../../../services/email.service";
import { RequiredValidator } from "../../../core/validators/required.validator";
import { EmailValidator } from "../../../core/validators/email.validator";
import { Environment } from "../../../core/models";
import { SharedService } from "../../services/shared.service";
import { LookupsModel } from "../../../core/interfaces";

import * as countries from 'country-list';

declare const Email: any;

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: [ './request.component.scss' ]
})
export class RequestComponent implements OnInit {

  public form: UntypedFormGroup;
  public showData: boolean;
  public baseURL = this.environment.webUrl;
  public countriesSelectOption: Array<LookupsModel>;
  public serviceTypeSelectOptions: Array<LookupsModel>;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private fb: UntypedFormBuilder,
    public translateLanguageService: TranslateLanguageService,
    private helperService: HelperService,
    private sharedService: SharedService,
    private emailService: EmailService,
    private environment: Environment
  ) {
  }

  ngOnInit(): void {
    console.log(Email);
    this.setSelectOptions();
    this.initForm();
  }

  private setSelectOptions(): void {
    this.countriesSelectOption = countries.getData();
    console.log(this.countriesSelectOption);
    this.serviceTypeSelectOptions = [
      { name: this.translateLanguageService.getTranslation('Client.Form.ServiceList.Accounting'), code: '1' },
      { name: this.translateLanguageService.getTranslation('Client.Form.ServiceList.LegalServices'), code: '2' },
      { name: this.translateLanguageService.getTranslation('Client.Form.ServiceList.Consulting'), code: '3' },
      { name: this.translateLanguageService.getTranslation('Client.Form.ServiceList.Relocation'), code: '4' },
    ];
  }

  private initForm(): void {
    this.form = this.fb.group({
      name: [ null, Validators.compose([ RequiredValidator.validate ]) ],
      surName: [ null, Validators.compose([ RequiredValidator.validate ]) ],
      email: [ null, Validators.compose([ RequiredValidator.validate, EmailValidator.validate ]) ],
      phone: [ null, Validators.compose([ RequiredValidator.validate ]) ],
      company: [ null, Validators.compose([ RequiredValidator.validate ]) ],
      country: [ null ],
      services: [ [] ],
      subject: [ null ],
    });
    console.log(this.countriesSelectOption);
    this.showData = true;
  }

  public onSubmit(): void {
    console.log(this.form.value);
    this.sharedService.showLoader$.next(true);
    let services = '';
    this.form.value.services.forEach((item: string, index: number) => {
      if (this.form.value.services.length > 1) {
        services += index === 0 ? item : ` ${this.translateLanguageService.getTranslation('And')} ` + item;
      } else {
        services = item;
      }
    });
    console.log(Email);
    Email.send({
      Host: 'smtp.elasticemail.com',
      Username: 'info@itreports.am',
      Password: 'BE888CB611653E95AA6B6C57F6B2ED793F22',
      To: 'info@itreports.am',
      From: `info@itreports.am`,
      Subject: 'Hello ITReports team',
      Body: `<table style="width:100%">
        <tr>
        <th>
        <h1>Hello ITReports team.</h1></th></tr>
        <tr>
            <td><strong>Name: </strong>${this.form.value.name}</td>
            <td><strong>SurName: </strong>${this.form.value.surName}</td>
        </tr>
        <tr><td><strong>Email: </strong>${this.form.value.email}</td></tr>
        <tr><td><strong>Phone: </strong>${this.form.value.phone}</td></tr>
        <tr><td><strong>Company: </strong>${this.form.value.company}</td></tr>
        <tr><td><strong>Country: </strong>${this.form.value.country}</td></tr>
        <tr><td><strong>Services: </strong>${services}</td></tr>
        </table>
        <p>${this.form.value.subject || ''}</p>
        <footer>I would like to connect with you.</footer>`
    }).then(message => {
      console.log(message);
      this.helperService.showSuccessMessage();
      this.sharedService.showLoader$.next(false);
      this.helperService.showPopup$.next(false);
      this.form.reset();
    });
    // this.emailService.sendEmail({ ...this.form.value, services })
    //   .subscribe(res => {
    //       this.helperService.showSuccessMessage();
    //       this.sharedService.showLoader$.next(false);
    //       this.helperService.showPopup$.next(false);
    //       this.form.reset();
    //     },
    //     error => {
    //       this.helperService.showSuccessMessage();
    //       this.sharedService.showLoader$.next(false);
    //       this.form.reset();
    //     });
  }

  close() {
    this.form.reset();
    this.helperService.showPopup$.next(false);
  }

}
