import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  public showLoader$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);

  constructor() { }
}
