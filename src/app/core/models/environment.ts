import { environment } from "../../../environments/environment";

export class Environment implements Readonly<typeof environment> {
  production: boolean;
  webUrl: string;
}
