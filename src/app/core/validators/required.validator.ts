import { AbstractControl } from '@angular/forms';

export class RequiredValidator {

  private static isEmptyInputValue(value: any): boolean {
    if (typeof value === 'string') {
      value = value.trim();
    }
    return value === undefined || value === null || value.length === 0;
  }

  static validate(control: AbstractControl) {
    return RequiredValidator.isEmptyInputValue(control.value) ? { required: true } : null;
  }

}
