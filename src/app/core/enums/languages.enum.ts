export enum Languages {
  'hy' = 1,
  'en' = 2,
  'ru' = 3,
}

export enum LanguageNames {
  '1_' = 'hy',
  '2_' = 'en',
  '3_' = 'ru',
}
