export interface LookupsModel {
  name: string;
  code: string;
  path?: string;
}
