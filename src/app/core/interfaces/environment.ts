import {environment} from '../../../environments/environment';

export interface Environment extends Readonly<typeof environment> {
  production: boolean;
  webUrl: string;

}
