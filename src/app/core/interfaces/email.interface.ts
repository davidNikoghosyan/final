export interface Email {
  name: string;
  email: string;
  location: string;
  subject: string;
}
